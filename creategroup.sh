#/bin/bash
aws deploy create-deployment-group \
--application-name fasthello \
--deployment-group-name fasthello_DepGroup \
--deployment-config-name CodeDeployDefault.OneAtATime \
--ec2-tag-filters Key=Deploy,Value=CodeDeploy,Type=KEY_AND_VALUE \
--service-role-arn arn:aws:iam::144988727007:role/CodeDeployRoleAPI
