#/bin/bash
aws deploy create-deployment \
--application-name fasthello \
--s3-location bucket=whd-codedeploy-new,key=fasthello.zip,bundleType=zip \
--deployment-group-name fasthello_DepGroup \
--ignore-application-stop-failures
